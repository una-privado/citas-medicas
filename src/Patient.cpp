#include "Patient.h"

Patient::Patient()
{
    id = "INDEFINIDO";
    name = "INDEFINIDO";
    lastName = "INDEFINIDO";
    age = 0;
}

Patient::Patient(string id,string name,string lastName,int age)
{
    this->id = id;
    this->name = name;
    this->lastName = lastName;
    this->age = age;
}

void Patient::setId(string id)
{
    this->id = id;
}

void Patient::setName(string name)
{
    this->name = name;
}

void Patient::setLastName(string lastName)
{
    this->lastName = lastName;
}

void Patient::setAge(int age)
{
    this->age = age;
}

string Patient::getId()
{
    return this->id;
}
string Patient::getName()
{
    return this->name;
}
string Patient::getLastName()
{
    return this->lastName;
}
int Patient::getAge()
{
    return this->age;
}

Patient::~Patient()
{
    //dtor
}
