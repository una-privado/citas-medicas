#ifndef COLLECTION_H
#define COLLECTION_H
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <string>
#include "Patient.h"

using namespace std;

class Collection : public Patient
{
    public:
        Collection();
        int convertHour(int hour);
        bool availability(int hour);
        void setAppointPeriod(int hour, bool hourChange);
        bool getAppointPeriod(int hour);
        void reservAppoint(int hour);
        string stateAppoint();
        int cantAvailability();
        void deleteAppoint(int hour);
        void addPatient(int hour,string id,string name,string lastName,int age);
        void showPatientHour(int hour);
        void showPatientId(string id);
        virtual ~Collection();

    protected:

    private:
        bool appointPeriod[12];
        Patient PatientAppoint[12];
};

#endif // COLLECTION_H
