#ifndef PATIENT_H
#define PATIENT_H
#include <string>

using namespace std;

class Patient
{
    public:
        Patient();
        Patient(string id,string name,string lastName,int age);
        void setId(string id);
        void setName(string name);
        void setLastName(string lastName);
        void setAge(int age);
        string getId();
        string getName();
        string getLastName();
        int getAge();
        virtual ~Patient();

    protected:

    private:
        string id;
        string name;
        string lastName;
        int age;


};

#endif // PATIENT_H
